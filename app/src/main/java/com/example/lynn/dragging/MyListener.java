package com.example.lynn.dragging;

import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import static com.example.lynn.dragging.MainActivity.*;

/**
 * Created by lynn on 2/18/2018.
 */

public class MyListener implements View.OnTouchListener {
    private float offSetX;
    private float offSetY;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)v.getLayoutParams();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            offSetX = event.getRawX() - layoutParams.leftMargin;
            offSetY = event.getRawY() - layoutParams.topMargin;
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            layoutParams.leftMargin = (int)(event.getRawX() - offSetX);
            layoutParams.topMargin = (int)(event.getRawY() - offSetY);

            v.setLayoutParams(layoutParams);
        }

        return(true);
    }

}
